<?php

namespace App\Tests\Unit\Service;

use App\Service\Beer\BeerCacheService;
use App\Service\Beer\GetBeerFromApiService;
use PHPUnit\Framework\TestCase;

class BeersUnitTest extends TestCase
{

    public function testGetBeerFromIdCache()
    {
        $beerCache = new BeerCacheService();
        $result = $beerCache->getBeer(1);
        $this->assertIsArray($result);
    }

    public function testGetBeerFromFoodCache()
    {
        $beerCache = new BeerCacheService();
        $result = $beerCache->getBeer('chicken');
        $this->assertIsArray($result);
    }

    public function testGetBeerIdFromApi()
    {
        $beerCache = new BeerCacheService();
        $beerApi = new GetBeerFromApiService($beerCache);
        $result = $beerApi->getBeerById(1);
        $this->assertEmpty($result);
    }

    public function testGetBeerFoodFromApi()
    {
        $beerCache = new BeerCacheService();
        $beerApi = new GetBeerFromApiService($beerCache);
        $result = $beerApi->getBeerByFood('chicken');
        $this->assertIsArray($result);
    }

}