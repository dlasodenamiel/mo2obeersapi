# Daniel Laso Denamiel
## Prueba técnica backend Simfony Rest Api

## Funciones

- Busqueda mediante una cadena de caracteres 
- Mostrar los datos de una cerveza especifica según el id especificado

Documentacion de la api en -> /api/doc


## Bundles
| Plugin | README |
| ------ | ------ |
| NelmioApiDocBundle | https://symfony.com/bundles/NelmioApiDocBundle/current/index.html

## Components
| Components | README |
| ------ | ------ |
| Filesystem  | https://symfony.com/doc/current/components/filesystem.html
| Serializer | https://symfony.com/doc/current/components/serializer.html

