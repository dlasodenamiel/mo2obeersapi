<?php

namespace App\Service;



use App\DTO\JSONResponse\CustomJsonResponse;
use App\DTO\JSONResponse\JsonMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;


class JsonResponseService
{

    /** @var SerializerInterface  */
    private SerializerInterface $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    /**
     * @param $data
     * @param $code
     * @param $message
     * @return Response
     */
    private function buildResponse($data, $code, $message): Response
    {
        $customJsonResponse = new CustomJsonResponse(new JsonMetadata($code, $message), $data);

        return new Response($this->serializer->serialize($customJsonResponse, 'json'));
    }

    /**
     * @param mixed $data
     * @param int $code
     * @param string $message
     * @return Response
     */
    public function buildSuccessResponse(mixed $data = '', int $code = Response::HTTP_OK, string $message = ''): Response
    {
        return $this->buildResponse($data, $code, $message);
    }

    /**
     * @param string $message
     * @param int $code
     * @param mixed $data
     * @return Response
     */
    public function buildErrorResponse(string $message = '', int $code = Response::HTTP_BAD_REQUEST, mixed $data = ''): Response
    {
        return $this->buildResponse($data, $code, $message);
    }

}