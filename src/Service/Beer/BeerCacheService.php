<?php

namespace App\Service\Beer;

use FileSystemCache;

FileSystemCache::$cacheDir = '/tmp/cache';

class BeerCacheService
{


    /**
     * @param $beer
     * @return void
     */
    public function setBeer($beer): void
    {
        $key = FileSystemCache::generateCacheKey('beers');
        $cachedBeers = FileSystemCache::retrieve($key);
        if ($cachedBeers !== false) {
            $beers = json_decode($cachedBeers, true);
        }
        $beers[] = $beer;
        FileSystemCache::store($key, json_encode($beers));
    }

    /**
     * @param $searchKey
     * @return mixed
     */
    public function getBeer($searchKey): mixed
    {
        $key = FileSystemCache::generateCacheKey('beers');
        $cachedBeers = FileSystemCache::retrieve($key);
        if ($cachedBeers !== false) {
            $beersFound = [];
            $beers = json_decode($cachedBeers, true);
            foreach ($beers as $beer) {
                if (gettype($searchKey) === 'integer') {
                    if ($beer['id'] === $searchKey) {
                        return $beer;
                    }else{
                        return null;
                    }
                } else {
                    $beerFound = true;
                    $searchKeys = explode(' ', $searchKey);
                    $searchWords = [];
                    foreach ($searchKeys as $key) {
                        $searchWords[strtolower($key)] = false;
                    }
                    foreach ($beer['food_pairing'] as $foodItem) {
                        foreach ($searchWords as $word => $found) {
                            if (str_contains($foodItem, $word)) {
                                $searchWords[$word] = true;
                            }
                        }
                    }
                    foreach ($searchWords as $found) {
                        if ($found === false) {
                            $beerFound = false;
                        }
                    }
                    if ($beerFound) {
                        $beersFound[] = $beer;
                    }
                }
            }
            return $beersFound;
        }
        return null;
    }
}