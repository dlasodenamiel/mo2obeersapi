<?php

namespace App\Service\Beer;


class GetBeerFromApiService
{
    const PUNK_API_URL = "https://api.punkapi.com/v2/";

    //URL to test api response
    const PUNK_API_MOCKED = "https://apimocha.com/testbeerapi/beer/1";

    private BeerCacheService $beerCacheService;

    /**
     * @param BeerCacheService $beerCacheService
     */
    public function __construct(
        BeerCacheService $beerCacheService
    )
    {
        $this->beerCacheService = $beerCacheService;
    }


    /**
     * @param $id
     * @return array|null
     */
    public function getBeerById($id): ?array
    {
//        $url = self::PUNK_API_URL . 'beers/' . $id;
        $url = self::PUNK_API_MOCKED . 'beers/' . $id;
        $punkApiResponse = $this->requestToApi($url);

        if ($punkApiResponse !== null && (!isset($punkApiResponse['statusCode']) || $punkApiResponse['statusCode'] !== 404)) {
            $this->beerCacheService->setBeer($punkApiResponse[0]);
            return $punkApiResponse[0];
        }
        return null;
    }

    /**
     * @param $food
     * @return array
     */
    public function getBeerByFood($food): array
    {
        $beers = [];
//        $url = self::PUNK_API_URL . 'beers?food=' . $food;
        $url = self::PUNK_API_MOCKED . 'beers?food=' . $food;
        $punkApiResponse = $this->requestToApi($url);

        if ($punkApiResponse !== null) {
            foreach ($punkApiResponse as $item) {
                $beer = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'tagline' => $item['tagline'],
                    'first_brewed' => $item['first_brewed'],
                    'description' => $item['description'],
                    'image' => $item['image_url'],
                ];
                $beers[] = $beer;
                $this->beerCacheService->setBeer($item);
            }
        }
        return $beers;
    }

    /**
     * @param $url
     * @return mixed
     */
    public function requestToApi($url): mixed
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        $punkApiResult = curl_exec($curl);
        curl_close($curl);
        return json_decode($punkApiResult, true);
    }
}