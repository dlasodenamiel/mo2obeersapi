<?php

namespace App\Service\ContentProvider;


use App\Service\Beer\BeerCacheService;
use App\Service\Beer\GetBeerFromApiService;

class BeerContentProvider
{
    private GetBeerFromApiService $getBeerFromApiService;

    private BeerCacheService $beerCacheService;


    /**
     * @param GetBeerFromApiService $getBeerFromApiService
     * @param BeerCacheService $beerCacheService
     */
    public function __construct(
        GetBeerFromApiService $getBeerFromApiService,
        BeerCacheService      $beerCacheService
    )
    {
        $this->getBeerFromApiService = $getBeerFromApiService;
        $this->beerCacheService = $beerCacheService;
    }

    public function getBeer($searchQuery)
    {
        $beer = $this->beerCacheService->getBeer($searchQuery);
        if ($beer === null) {
            if (gettype($searchQuery) === 'integer') {
                $beer = $this->getBeerFromApiService->getBeerById($searchQuery);
            } else {
                $beer = $this->getBeerFromApiService->getBeerByFood($searchQuery);
            }
        }
        return $beer;
    }
}