<?php

namespace App\DTO\JSONResponse;

class JsonMetadata
{
    /** @var int */
    private int $code;

    /** @var string */
    private string $message;

    /**
     * @param int $code
     * @param string $message
     */
    public function __construct(int $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): JsonMetadata
    {
        $this->code = $code;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): JsonMetadata
    {
        $this->message = $message;
        return $this;
    }
}