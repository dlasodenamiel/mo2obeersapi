<?php

namespace App\DTO\JSONResponse;

class CustomJsonResponse
{
    /** @var JsonMetadata */
    private JsonMetadata $metadata;

    /** @var mixed */
    private mixed $data;

    /**
     * @param JsonMetadata $metadata
     * @param mixed $data
     */
    public function __construct(JsonMetadata $metadata, mixed $data)
    {
        $this->metadata = $metadata;
        $this->data = $data;
    }

    public function getMetadata(): JsonMetadata
    {
        return $this->metadata;
    }

    public function setMetadata(JsonMetadata $metadata): CustomJsonResponse
    {
        $this->metadata = $metadata;
        return $this;
    }

    public function getData(): mixed
    {
        return $this->data;
    }

    public function setData(mixed $data): CustomJsonResponse
    {
        $this->data = $data;
        return $this;
    }

}