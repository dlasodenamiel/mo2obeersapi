<?php


namespace App\Controller;

use App\Service\ContentProvider\BeerContentProvider;
use App\Service\JsonResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BeersController extends AbstractController
{

    /**
     * @param BeerContentProvider $beerContentProvider
     * @param JsonResponseService $jsonResponseService
     */
    public function __construct(
        private BeerContentProvider $beerContentProvider,
        private JsonResponseService $jsonResponseService,
    )
    {
    }


    /**
     * @param $id
     * @return Response
     */
    #[Route('/beer/{id}', name: 'app_beers_getbeerbyid', requirements: ['id' => '\d+'], methods: ['GET'])]
    public function getBeerById($id): Response
    {
        $beerResponse = $this->beerContentProvider->getBeer(intval($id));
        if ($beerResponse === null) {
            return $this->jsonResponseService->buildErrorResponse('Beer not found', Response::HTTP_NOT_FOUND);
        } else {
            $beer = [
                'id' => $beerResponse['id'],
                'name' => $beerResponse['name'],
                'tagline' => $beerResponse['tagline'],
                'first_brewed' => $beerResponse['first_brewed'],
                'description' => $beerResponse['description'],
                'image' => $beerResponse['image_url'],
            ];
            return $this->jsonResponseService->buildSuccessResponse($beer);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    #[Route('/beer', name: 'app_beers_getbeerbyfood', methods: ['POST'])]
    public function getBeerByFood(Request $request): Response
    {

        $data = json_decode($request->getContent(), true);
        if (!isset($data['food'])){
            return $this->jsonResponseService->buildErrorResponse('Bad Request');
        }
        $food = $data['food'];
        $food = str_replace(' ', '', $food);
        $food = str_replace(',', '_', $food);

        $beersResponse = [];
        $beers = $this->beerContentProvider->getBeer($food);
        foreach ($beers as $item) {
            $beer = [
                'id' => $item['id'],
                'name' => $item['name'],
                'tagline' => $item['tagline'],
                'first_brewed' => $item['first_brewed'],
                'description' => $item['description'],
                'image' => $item['image_url'],
            ];
            $beersResponse[] = $beer;
        }

        if ($beersResponse === null || count($beersResponse) === 0) {
            return $this->jsonResponseService->buildErrorResponse('Beer not found', Response::HTTP_NOT_FOUND);
        } else {
            return $this->jsonResponseService->buildSuccessResponse($beersResponse);
        }
    }
}
